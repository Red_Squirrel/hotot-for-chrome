var hototURL = chrome.extension.getURL("index.html");	
chrome.tabs.query({url: hototURL}, function(tabs) {
		if (tabs.length > 0) {
			chrome.tabs.update(tabs[0].id, {selected: true});
			var v =	null;
			var views = chrome.extension.getViews();
			for (var i=0; i<views.length; i++) {
				if (views[i].location.href.indexOf(hototURL) !== -1) {
					v = views[i];
					break;
				}
			}
			if (v && v.globals) {
				v.ui.PrefsDlg.load_settings(v.conf.settings);
				v.ui.PrefsDlg.load_prefs();
				v.globals.prefs_dialog.open();
			}			
		} else {
			chrome.tabs.create({url: hototURL+"#options"});
		}	
		window.close();
});
