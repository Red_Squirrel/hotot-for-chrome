// 4.0: Notifications Settings
var notification = {

init:
function init() {
	if (conf.vars.platform === 'Chrome') {
		chrome.notifications.onClicked.addListener(function(nid) {
			if (conf.get_current_profile().preferences.notification_clear_onclick) 
				chrome.notifications.clear(nid, function(wasCleared) {});
		});
	}
},

notify_tweet:
function notify_tweet(tweet) {
	var prefs = conf.get_current_profile().preferences;
		
	var media;
	if (tweet.extended_entities && tweet.extended_entities.media && prefs.notification_show_tweet_media) {
		media = tweet.extended_entities.media[0].media_url;
	}
	
	//Check if DM
	var usertype, dm = '';
	if (tweet.hasOwnProperty('user')) {
		usertype = tweet.user;
		dm = '';
	} else {
		usertype = tweet.sender;
		dm = '[DM] ';
	}
	
	var options = {
		type: (media ? "image" : "basic"),
		title: dm + ui.Template.parse_name(usertype.name,usertype.screen_name),
		message: (prefs.notification_show_tweet_msg ? tweet.text : _('notification_message')),
		iconUrl: (prefs.notification_show_user_pic ? usertype.profile_image_url.replace("_normal","") : '../image/ic128_notification.png'),
		isClickable: true
	}
	
	if (media) {
		$.extend(options, {
			imageUrl: media
		});
	}	
	
	notification.create(options);
},

create:
function create(options) {
	var prefs = conf.get_current_profile().preferences;
	if (conf.vars.platform === 'Chrome') {
		chrome.notifications.create("", options, function creationCallback(id) {
			if (prefs.notification_auto_clear) {
				setTimeout(function() { 
					chrome.notifications.clear(id, function(wasCleared) {}); 
					}, 
					(prefs.notification_auto_clear_time * 1000)
				);
			}
		});	
	}
},

notify_text:
function notify_text(title, text, image) {
    title = title.replace(/&gt;/g, '>').replace(/&lt;/g, '<').replace(/&amp;/g, '&');
    text = text.replace(/&gt;/g, '>').replace(/&lt;/g, '<').replace(/&amp;/g, '&');
    var prefs = conf.get_current_profile().preferences;		
    var options = {
	type: "basic",
	title: title,
	message: text,
	iconUrl: ((prefs.notification_show_user_pic && image) ? image.replace("_normal","") : '../image/ic128_notification.png'),
	isClickable: true
    }
    notification.create(options);
},

notify_error:
function notify_error(text) {
    text = text.replace(/&gt;/g, '>').replace(/&lt;/g, '<').replace(/&amp;/g, '&');
    var options = {
	type: "basic",
	title: _('oops_an_error_occurs'),
	message: text,
	iconUrl: '../image/ic128_error.png',
	isClickable: true
    }
    notification.create(options);
}

};
