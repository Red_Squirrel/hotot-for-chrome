if (typeof ui == 'undefined') var ui = {};
ui.DMTabs = {

current: null,

json: [], // 2.2: to avoid new Twitter requests every time

current_view: null, // 2.2: to avoid to check for current view every time

// 2.2: DM get/sent fix!
max_id_dm_get: null,
max_id_dm_sent: null,

views: { // 2.2: to avoid to rewrite header every time
},

// 4.0: Optimizations
dm_array: [],

init:
function init() {
    var btns = new widget.RadioGroup('#dm_radio_group');
    btns.on_clicked = function (btn, event) {
        ui.DMTabs.current = $(btn).attr('href');
        var page_name = ui.DMTabs.current + '_sub_block';
        $('#direct_messages_tweet_block .tweet_sub_block').not(page_name).hide();
        $(page_name).show();
    };
    btns.create();
    ui.DMTabs.current = '#direct_messages_inbox';
    $(ui.DMTabs.current + '_sub_block').show();
},

//2.2: New DM view
init_view:
function init_view(view) {
	ui.DMTabs.reset(); // Reinitializes vars because they could be not empty if previous user did not use proper methods to exit
	ui.DMTabs.load_DM_header(view, null, null);
},

// 4.0: Optimizations
destroy_view:
function destroy_view(view) {
	ui.DMTabs.reset();	
	ui.Slider.remove(view.name);
},

reset:
function reset() {
	ui.DMTabs.json = [];
	ui.DMTabs.views = {};
	ui.DMTabs.current_view = null;
	ui.DMTabs.max_id_dm_get = null;
	ui.DMTabs.max_id_dm_sent = null;
},

load_DM_header:
function load_DM_header(view, success, fail) {
	var h1 = '<div class="header_frame" style="text-align:left;">\
	<ul class="mochi_list">\
		<li class="mochi_list_item with_trigger">\
			<a id="btn_dm_any" class="trigger mochi_dm page_nav">\
				<span class="widget more"></span>\
				<label for="" class="label" data-i18n-text="any" style="font-weight:bold;font-size:13px;">'+_('any')+'</label>\
			</a>\
		</li>';
	
	for (var user in ui.DMTabs.views) {
		h1 += '<li class="mochi_list_item with_trigger" >\
				<a class="trigger mochi_dm page_nav btn_dm">\
					<span class="widget more"></span>\
					<img src="'+ui.DMTabs.views[user]+'" style="float:left;height:30px;margin-right:10px;border-radius:5px;"/>\
					<label for="" class="label" style="font-weight:bold;font-size:13px;">'+user+'</label>\
					<span id="ic_new_message" style="display:none;">\
					<img data-i18n-title="new_messages" title="'+_('new_messages')+'" src="../image/ic_new_message.png" style="float:right;height:30px;margin-right:10px;" />\
					</span>\
				</a>\
			</li>';
	}
			
	h1 += '</ul>\
	<div align="right"><button id="dm_load_more" data-i18n-text="load_more" class="mochi_button" style="margin-top:5px;font-weight:bold;">'+_('load_more')+'</button></div>\
	</div>';	
	
	var h2 = '<div class="sub_header_frame" style="text-align:left;display:none;"><div class="dm_view_users">\
			<button id="dm_write" data-i18n-text="compose" class="mochi_button blue" style="font-weight:bold;">'+_('compose')+'</button>\
			<button id="btn_dm_back" data-i18n-text="back" class="mochi_button red" style="float:right;font-weight:bold;">'+_('back')+'</button>\
			</div></div>';
	
	view._header.children('.header_content').children('.header_frame').html(h1);
	view._header.children('.header_content').children('.sub_header_frame').html(h2); // for now h2 does not really need to be replaced every time but it could become useful in future
	if (ui.DMTabs.current_view != null) { //Fix for when you are not in main page and receive a DM from a new user
		view._header.find('.header_frame').hide();
		view._header.find('.sub_header_frame').show();	
	} 
	//Fix for scrollbar
	view.scrollbar.recalculate_layout();
	
	// Set buttons click events
	view._header.find('#btn_dm_back').click(function() {		
		ui.DMTabs.current_view = null;
		view._header.find('.sub_header_frame').hide();
		view._header.find('.header_frame').show();
		view.clear();
	});
	
	view._header.find('#btn_dm_any').click(function() {
		ui.DMTabs.current_view = $(this).text().trim();
		view._header.find('.header_frame').hide();
		view._header.find('.sub_header_frame').show();				
		view.clear();
		ui.Main.add_tweets(view, ui.DMTabs.json, false);
		view.scrollbar.recalculate_layout();
	});
				
	view._header.find('.btn_dm').click(function() {
		ui.DMTabs.current_view = $(this).text().trim();
		$(this).find('#ic_new_message').hide();
		view._header.find('.header_frame').hide();
		view._header.find('.sub_header_frame').show();				
		view.clear();
		ui.Main.add_messages(view, ui.DMTabs.json, $(this).text().trim());
		view.scrollbar.recalculate_layout();
	});	
	
	$('#dm_load_more').click(function(event) {
		toast.set(_('loading_messages')).show();
		ui.Main.loadmore_messages_new(view);
	});

	$('#dm_write').click(function(event) {
		if (ui.DMTabs.current_view != _('any')) {
			ui.StatusBox.set_dm_target(ui.DMTabs.current_view);
		} else {
			ui.StatusBox.set_dm_target("");
		}
		ui.StatusBox.set_status_text('');
		ui.StatusBox.open(function () {
			ui.StatusBox.change_mode(ui.StatusBox.MODE_DM);
			ui.StatusBox.move_cursor(ui.StatusBox.POS_END);
		});
	})
},

show_button_notify:
function show_button_notify(view, screen_name) {
	if (screen_name != ui.DMTabs.current_view) {
		var buttons = view._header.find('.btn_dm');
		for (var y = 0; y < buttons.length; y++) {
			if ($(buttons[y]).text().trim() == screen_name) {
				$(buttons[y]).find('#ic_new_message').show();
				break;
			}
		}
	}
}

};
