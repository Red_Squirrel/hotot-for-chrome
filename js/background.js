var hototURL = chrome.extension.getURL("index.html");
var context_menu = false;

function onExtMessage(req, sender, response) {
	if (req.enableContextMenu) {
		EnableContextMenu();
		response({'reply': 'Enabled'});
	} else {
		DisableContextMenu();
		response({'reply': 'Disabled'});
	}
	chrome.storage.sync.set({enabled: req.enableContextMenu}, null);
}

function sharePage(info, tab) {
	ShareWithHotot(tab.title + ' ' + info.pageUrl);
}

function shareSelection(info, tab) {
	ShareWithHotot("\"" + info.selectionText + "\" via: " + info.pageUrl);
}

function shareLink(info, tab) {
	ShareWithHotot(info.linkUrl);
}
  
function EnableContextMenu() {
    if (context_menu === false) {
	context_menu = true;
	chrome.contextMenus.create({
	        "title": chrome.i18n.getMessage('share_page'),
	        "contexts": ["page"],
		"onclick": sharePage
	});
	chrome.contextMenus.create({
	        "title": chrome.i18n.getMessage('share_selection'),
	        "contexts": ["selection"],
		"onclick": shareSelection
	});
	chrome.contextMenus.create({
		"title": chrome.i18n.getMessage('share_link'),
	        "contexts": ["link"],
	        "onclick": shareLink
	});
    }
}

function DisableContextMenu() {
	chrome.contextMenus.removeAll();
	context_menu = false;
}

function ShareWithHotot(text) {
		var v =	null;
		var views = chrome.extension.getViews();
		for (var i=0; i<views.length; i++) {
			if (views[i].location.href.indexOf(hototURL) !== -1) {
				v = views[i];
				break;
			}
		}
		if (v && v.globals) {
			if (v.globals.signed_in) {
				GoToHototPlus();
				v.ui.StatusBox.set_status_text(text);
				v.ui.StatusBox.open(function() {
					v.ui.StatusBox.move_cursor(v.ui.StatusBox.POS_BEGIN);
					v.ui.StatusBox.change_mode(v.ui.StatusBox.MODE_TWEET);
					v.ui.StatusBox.update_status_len();
				});
			} else {
				var options = {
					type: "basic",
					title: chrome.i18n.getMessage('oops_not_signed'),
					message: chrome.i18n.getMessage('sign_in_to_share'),
					iconUrl: "../image/ic128_hotot.png"
				}
				chrome.notifications.create("", options, function creationCallback(id) {});
			}
		} else {
			var options = {
				type: "basic",
				title: chrome.i18n.getMessage('oops_hotot_not_running'),
				message: chrome.i18n.getMessage('start_hotot'),
				iconUrl: "../image/ic128_hotot.png"
			}
			chrome.notifications.create("", options, function creationCallback(id) {});	
		}		
}
  
function GoToHototPlus() {
	chrome.tabs.query({url: hototURL}, function(tabs) {
		if (tabs.length > 0) {
			chrome.tabs.update(tabs[0].id, {selected: true});
		} else {
			chrome.tabs.create({url: hototURL});
		}

	});
}
  
chrome.browserAction.onClicked.addListener(GoToHototPlus);
chrome.extension.onMessage.addListener(onExtMessage);
chrome.storage.sync.get({enabled: false}, function(result) {
	if (result.enabled) EnableContextMenu();
});