if (typeof ui == 'undefined') var ui = {};
ui.HomeTabs = {

current_filter: null,

// 4.0: Home media-only mode
init:
function init() {
	ui.HomeTabs.current_filter = null;
},

init_view:
function init_view(view) {
    ui.HomeTabs.current_filter = null;
    var toggle = view._header.find('.home_view_toggle_btns');
    var sub_view_btns = toggle.find('.radio_group_btn');
    sub_view_btns.click(function (event) {
	if (!($(this).hasClass('selected'))) {
		// Get filter
		var pagename = $(this).attr('id');
		ui.HomeTabs.current_filter = pagename;
		
		// Change selected radio button
		sub_view_btns.removeClass('selected');
		$(this).addClass('selected');
			
		// Apply filter
		ui.HomeTabs.filter_timeline();
	}
    });
},

load_tweet_success:
function load_tweet_success(self, json) {   
    
    // Filter for only media:
    if (ui.HomeTabs.current_filter == 'home_media_only') {
	var media_json = [];
	for (var i=0; i<json.length; i+=1) {
		if( (json[i].entities && json[i].entities.media) || (json[i].extended_entities && json[i].extended_entities.media) )
			media_json.push(json[i]);
	} 
	json = media_json;
    }
    
    var ret = ui.Main.add_tweets(self, json, false);
    
    if ((ret == 0) || (self.incoming_num <= 0)) { // Nothing to notify
        return json.length;
    }
    
    hotot_log('incoming_num of '+self.name, self.incoming_num);
    
    // Notify!
    ui.Slider.set_unread(self.name);
    if (ui.Main.views[self.name].use_notify) {
	// Check for too notifications
	if (self.incoming_num > 3) {
		notification.notify_text(self.incoming_num + " " + _('new_notifications'), _('many_notifications_on_page') + " " + self.name, null);
	} else {
		for (var i = 0; i < self.incoming_num; i += 1) {
			notification.notify_tweet(json[i]);
		}
	}
        unread_alert(self.incoming_num);
    }
    if (ui.Main.views[self.name].use_notify_sound) {
            $('#audio_notify').get(0).play();
    }

    ui.HomeTabs.resize_images_preview();
    return json.length;
},

loadmore_tweet_success:
function loadmore_tweet_success(self, json) {

    // Filter for only media:
    if (ui.HomeTabs.current_filter == 'home_media_only') {
	var media_json = [];
	for (var i=0; i<json.length; i+=1) {
		if( (json[i].entities && json[i].entities.media) || (json[i].extended_entities && json[i].extended_entities.media) ) {
			media_json.push(json[i]);
		}
	} 
	self.max_id = json[json.length-1].id; // Fix for max_id if no new tweet contains media...
	json = media_json;
    }
    
    var ret = ui.Main.add_tweets(self, json, true);
    if (0 < self.incoming_num) {
        ui.Slider.set_unread(self.name);
    }
    
    ui.HomeTabs.resize_images_preview();
    return ret;
},

filter_timeline:
function filter_timeline() {
	var tweets = $('#home_tweetview .card');
	if (ui.HomeTabs.current_filter == 'home_media_only') {
		$('a[href="#home"] .icon').attr('src', '../image/ic_archive_alt.png');
		$('a[href="#home"] .icon_alt').css('background-image','url(../image/ic_archive_alt.png)');	
		var c = tweets.length-1;
		for (var i = 0; i < tweets.length; i += 1) {
			if ($(tweets[i]).find('div.preview').children().length === 0) {
				$(tweets[i]).remove();
				c -= 1;
			}
		}
		if (c < 10) { 
			ui.Main.views['home'].loadmore();
		}
		ui.HomeTabs.resize_images_preview();
	} else {
		$('a[href="#home"] .icon').attr('src', '../image/ic_home.png');
		$('a[href="#home"] .icon_alt').css('background-image','url(../image/ic_home.png)');
		ui.HomeTabs.resize_images_preview();
		ui.HomeTabs.reload_full_timeline();
	}
},

reload_full_timeline:
function reload_full_timeline() {
	ui.Main.views['home'].clear();
	ui.Main.views['home'].max_id = null;
	ui.Main.views['home'].since_id = null;
	ui.Main.views['home'].load();
},

resize_images_preview:
function resize_images_preview() {
	if (ui.HomeTabs.current_filter === 'home_media_only') {
		$('#home_tweetview .media_preview').each(function(i,val) {
			var a = $(val).find('a');
			var direct_url = a.attr('direct_url');
			var youtube_id = a.attr('youtube_id');
			$(val).find('img').attr('src',direct_url).css('max-height','none');
			if (youtube_id && !conf.get_current_profile().preferences.show_youtube) {
				$(val).append('<iframe class="yt_iframe"\
				width="' + (globals.tweet_block_width-100) + '" \
				height="' + ((globals.tweet_block_width-100)/16*9+32) + '" \
				src="http://www.youtube.com/embed/' + youtube_id + '?rel=0" frameborder="0" allowfullscreen></iframe>');
			}
		});
	} else {
		$('#home_tweetview .media_preview img').css('max-height','150px');
		if (!conf.get_current_profile().preferences.show_youtube) {
			$(".yt_iframe").remove();
		}
	}
}

};
