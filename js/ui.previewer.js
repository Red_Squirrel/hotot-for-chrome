(function() {
  var Previewer, root, _ref;

  Previewer = (function() {

    Previewer.name = 'Previewer';

    function Previewer(sel) {
      var _this = this;
      this.me = $(sel);
      this.image = this.me.find('.image');
      this.link = this.me.children('.image_wrapper');
      this.video = this.me.find('.ytframe');
      this.close_btn = this.me.children('.close');
      this.visible = false;
      this.close_btn.click(function() {
        return _this.close();
      });
      this.link.click(function(ev) {
        if (ev.which !== 1 && ev.which !== 2) {
          _this.close();
          return;
        }
        if (conf.vars.platform === 'Chrome') {
          chrome.tabs.create({
            url: _this.link.attr('href'),
            active: ev.which === 1
          }, function() {});
          _this.close();
          return false;
        }
        return _this.close();
      });
      //Init
      this.reset();
    }

    Previewer.prototype.reset = function() {
	this.image.hide();
	this.video.hide();
	this.video.attr('width','160');
	this.video.attr('height','24');
	this.video.attr('src','../image/ani_loading_bar.gif');
	this.image.attr('src', '../image/ani_loading_bar.gif');
	this.image.css('margin', '0');
        this.image.width(160);
        this.image.height(24);
        this.resize(160, 24);	
	return this.image.css('margin', '20px 0');
    };

    Previewer.prototype.reload = function(image_url) {
      this.reset();
      return this.reload_proc(image_url);
    };

    Previewer.prototype.reload_proc_video = function(src) { //4.0: Dailymotion Support
	var width = 640;
        var height = 360;
        if ($(window).width() < width + 40) {
          width = $(window).width() - 40;
          height = (width + .0) / 640 * 360;
        }
        if ($(window).height() < height + 70) {
          height = $(window).height() - 70;
          width = (height + .0) / 360 * 640;
        }
	this.video.attr('src',src);	
	this.video.attr('width',width);
	this.video.attr('height',height);
        this.resize(width, height+8);
	this.image.hide();
	this.video.show();
	return true;
    };
    
    Previewer.prototype.reload_proc = function(image_url) {
      var preloader,
        _this = this;
	_this.video.hide();
	_this.image.show();
      preloader = new Image;
      preloader.onload = function() {
        var height, preload, width;
        _this.image.attr('src', image_url);
        _this.image.css('margin', '0');
        width = preloader.width;
        height = preloader.height;
        if ($(window).width() < width + 40) {
          width = $(window).width() - 40;
          height = (width + .0) / preloader.width * preloader.height;
        }
        if ($(window).height() < height + 70) {
          height = $(window).height() - 70;
          width = (height + .0) / preloader.height * preloader.width;
        }
        _this.image.width(width);
        _this.image.height(height);
        _this.link.attr('href', image_url);
        _this.resize(width, height);
        return preload = null;
      };
      return preloader.src = image_url;
    };

    Previewer.prototype.resize = function(width, height) {
      if (width < 64) {
        width = 64;
      }
      if (height < 64) {
        height = 64;
      }
      height += 30;
      this.me.width(width).height(height);
      return this.me.css({
        'margin-top': (0 - height) / 2 - 10,
        'margin-left': (0 - width) / 2 - 10
      });
    };

    Previewer.prototype.open = function() {
      this.visible = true;
      this.me.show();
      return this.me.transition({
        'opacity': 1
      }, 100);
    };

    Previewer.prototype.close = function() {
      var _this = this;
      this.reset();
      this.visible = false;
      return this.me.transition({
        'opacity': 0
      }, 100, function() {
        return _this.me.hide();
      });
    };

    return Previewer;

  })();

  root = typeof exports !== "undefined" && exports !== null ? exports : this;

  root.widget = (_ref = root.widget) != null ? _ref : {};

  root.widget.Previewer = Previewer;

}).call(this);
