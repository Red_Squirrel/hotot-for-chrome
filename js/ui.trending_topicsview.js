if (typeof ui == 'undefined') var ui = {};
ui.TrendingTopicsView = {

woeid: 1,

init:
function init() {
    var btns = new widget.RadioGroup('#trend_topics_radio_group');
    btns.on_clicked = function (btn, event) {
        ui.TrendingTopicsView.current = $(btn).attr('href');
        toast.set(_('loading_tweets')).show();
    };
    btns.create();
},

init_view:
function init_view(view) {
    ui.TrendingTopicsView.woeid = conf.get_current_profile().preferences.tt_woeid;
    var vcard = view._header.find('.tt_vcard');
    view._header.find('.expand').click(function () {
        if (vcard.is(':hidden')) {
            vcard.slideDown('fast');
        } else {
            vcard.slideUp('fast');
        }
    });
    
    ui.TrendingTopicsView.get_countries(view);
    
    var toggle = view._header.find('#sel_country');
    toggle.change(function (event) {
	ui.TrendingTopicsView.woeid = toggle.val();
	conf.get_current_profile().preferences.tt_woeid = ui.TrendingTopicsView.woeid;
	conf.save_prefs(conf.current_name);
	view.load();
    });    
},

compare:
function compare(a,b) {
	var na = a.country +' ('+a.name+')', nb = b.country +' ('+b.name+')';
	if (na < nb) return -1;
	if (na > nb) return 1;
	return 0;
},

get_countries:
function get_countries(view){
	globals.twitterClient.get_trending_topics_available(function(result) {
		var h = "";
		result.sort(ui.TrendingTopicsView.compare);
		for (var place in result) {
			h += '<option value="'+result[place].woeid+'">'+result[place].country +' ('+result[place].name+')</option>';
		}
		view._header.find('#sel_country').html(h);
		view._header.find('#sel_country').val(ui.TrendingTopicsView.woeid);
	});
},

load_tt:
function load_tt(view, success) {
	var woeid = ui.TrendingTopicsView.woeid;
	if (!(woeid > 0)) woeid = 1;
	globals.twitterClient.get_trending_topics(woeid, success);
},

load_tt_success:
function load_tt_success(self, json) {
	var trends = json[0].trends;
	var h = '<ul class="mochi_list dark">';
	for (var i in trends) {
		h += '<li class="mochi_list_item with_trigger" >\
				<a class="trigger dark btn_tt">\
					<label for="" class="label" style="font-weight:bold;font-size:13px;">'+trends[i].name+'</label>\
				</a>\
			</li>';
	}
	h+="</ul>";
	self._header.find('.tt_list').html(h);	
	self._header.find('.btn_tt').click(function(){
		ui.Slider.addDefaultView('search', {}) || ui.Slider.add('search');
		ui.Main.views.search._header.find('.search_entry').val($(this).text().trim());
		ui.Main.views.search._header.find('.search_tweet').click();
	});
}

};
