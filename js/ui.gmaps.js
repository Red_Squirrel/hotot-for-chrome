if (typeof ui == 'undefined') var ui = {};
ui.GMaps = {

map_doc: null,

map_dialog: null,

map_zoom: 14, 
map_x: null,
map_y: null,
map_type: 'roadmap',


init:
function init () {
    var width = $(window).width() / 2;
    var height = $(window).height() / 2;
    var id = '#ext_hotot_gmap_map_dialog';
    ui.GMaps.map_dialog = widget.DialogManager.build_dialog(
	    id
            , 'Google Maps', 
	    '', 
	    '<iframe id="hotot_gmap_frame" class="dialog_body" width="' + width + '" height="' + height + '"></iframe>'
            , 
	    [{
		'id': id + '_more_btn', 
		label: 'Zoom +', 
		click: function (event) {
                    ui.GMaps.on_map_indicator_clicked(null, null, 100);
		}
	    },
	    {
		'id': id + '_less_btn', 
		label: 'Zoom -', 
		click: function (event) {
		    ui.GMaps.on_map_indicator_clicked(null, null, -100);
		}
	    },
	    {
		'id': id + '_type_btn', 
		label: 'Change Map Type', 
		click: function (event) {
		    ui.GMaps.on_map_indicator_clicked(null, null, ui.GMaps.map_zoom, true);
		}
	    }]
    );
    ui.GMaps.map_dialog.set_styles('header', {'padding': '0', 'height': '0', 'display': 'none'});   
    ui.GMaps.map_dialog.set_styles('body', {'padding': '0'});
    ui.GMaps.map_doc = $('#hotot_gmap_frame').get(0).contentWindow.document;
    ui.GMaps.map_doc.open();
    ui.GMaps.map_doc.write("<html><head><style>*{margin: 0 0 0 0;padding:0 0 0 0;overflow:hidden;}</style></head><body><div id=\"map\" style=\"width:100%;height:100%;\"></div></body></html>");
    ui.GMaps.map_doc.close();
},

on_form_indicator:
function on_form_indicator(tweet) {
    if (tweet.geo && tweet.geo.type == 'Point') {
        var x = tweet.geo.coordinates[0];
        var y = tweet.geo.coordinates[1];
        var tag = "geo1_" + tweet.id + Date.now();
        
        setTimeout(function() {
            $('#' + tag).click(function(e) {
                e.preventDefault();
                return ui.GMaps.on_map_indicator_clicked(x, y);
            })
        }, 500);
        return '<a class="geo_indicator" href="#" x="'+x+'" y="'+y+'" id="' + tag + '" style="background: transparent url(../image/ic16_marker.png) no-repeat; width: 18px; height: 18px; display:inline-block;"></a>';
    } else {
	return '';
    }
},

on_map_indicator_clicked:
function on_map_indicator_clicked(x, y, zoom, type) {
    ui.GMaps.map_dialog.open(); 

    if (x != null) ui.GMaps.map_x = x;
    if (y != null) ui.GMaps.map_y = y;
    if (zoom == undefined) {
	ui.GMaps.map_zoom = 14;
    } else if (zoom != ui.GMaps.map_zoom) {
	if (zoom < 0) { 
		ui.GMaps.map_zoom = (ui.GMaps.map_zoom < 2) ? 1 : (ui.GMaps.map_zoom - 1);
	} else {
		ui.GMaps.map_zoom = (ui.GMaps.map_zoom > 20) ? 21 : (ui.GMaps.map_zoom + 1);
	}
    }
    if (type != undefined) {
	switch (ui.GMaps.map_type) {
	    case "roadmap":
		ui.GMaps.map_type = "satellite";
	    break;
	    case "satellite":
		ui.GMaps.map_type = "terrain";
	    break;
	    case "terrain":
		ui.GMaps.map_type = "hybrid";
	    break;
	    case "hybrid":
		ui.GMaps.map_type = "roadmap";
	    break;
	    default:
		ui.GMaps.map_type = "roadmap";
	    break;
	}
    }
    
    $('#hotot_gmap_frame').get(0).contentWindow.document.getElementById('map').innerHTML = 
    '<img src="https://maps.googleapis.com/maps/api/staticmap?' +
    'markers=' + ui.GMaps.map_x + ',' + ui.GMaps.map_y +
    '&visual_refresh=true' + 
    '&zoom=' + ui.GMaps.map_zoom +
    '&size=' + $('#ext_hotot_gmap_map_dialog').width() + 'x' + $('#ext_hotot_gmap_map_dialog').height() +
    '&maptype=' + ui.GMaps.map_type +
    '&language=en' +
    '&sensor=true' +
    '&key=AIzaSyBXoNxopCyNLvN8qR6Xr1fhm1JJw6_Gnys' + 
    '" ' +
    'height="' + $('#ext_hotot_gmap_map_dialog').height() + 'px" width="' + $('#ext_hotot_gmap_map_dialog').width()+'px" >';
}


}

